COMP 4321 Project
Ng Yee Ting <ytngaa@ust.hk>, Wong Kwok Shiu <kswongab@ust.hk>, Wong Shirley Mei Wah <smwwong@ust.hk>

This project is managed by maven.

To build the project, use:
mvn package

This will generate the WAR file and exploded directory for the web application,
which can then be deployed to Tomcat.

To run the spider, go to the directory of the web application, and use:
java -cp .:./WEB-INF/classes:./WEB-INF/lib/* comp4321.Spider http://www.cse.ust.hk/~ericzhao/COMP4321/TestPages/testpage.htm 300 ./database/db

The above command will run the spider that begins at http://www.cse.ust.hk/~ericzhao/COMP4321/TestPages/testpage.htm and indexes 300 pages.

To use the web interface for search engine,
visit comp4321proj.html from your web server.

Please see the documentation for more information.
