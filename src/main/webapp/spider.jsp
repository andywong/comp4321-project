<%@ page import="java.io.File" %>
<%@ page import="comp4321.utilities.StopStem" %>
<%@ page import="comp4321.utilities.PorterStopStem" %>
<%@ page import="comp4321.*" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.MalformedURLException" %>
<%@ page import="javax.servlet.jsp.JspWriter" %>

<!DOCTYPE html>
<html>
<head>
    <title>COMP4321 Project - Search Results</title>
</head>
<body>
<%
    String url = "http://www.cse.ust.hk/~ericzhao/COMP4321/TestPages/testpage.htm";
    int limit = 300;

    if("true".equals(request.getParameter("run"))) {
        out.println("Crawling... <br/>");

        String jspFilePath = getServletContext().getRealPath(request.getServletPath());
        File folder = new File(new File(jspFilePath).getParent(), "/database");
        folder.mkdirs();
        final Database db = new Database(new File(folder, "/db"));
        StopStem stemmer = new PorterStopStem(Spider.class.getResourceAsStream("stopwords.txt"));
        Crawler crawler = new BreadthFirstCrawler(limit);

        final JspWriter output = out;

        Indexer indexer = new Indexer(crawler, db, stemmer);
        indexer.setListener(new Indexer.IndexListener() {
            @Override
            public void added(long id) {
                try {
                    output.println("Indexed " + db.getPageUrl(id) + "<br/>");
                    output.flush();
                } catch (Exception e) {
                }
            }

            @Override
            public void updated(long id) {
                try {
                    output.println("Updated index for " + db.getPageUrl(id) + "<br/>");
                    output.flush();
                } catch (Exception e) {
                }
            }

            @Override
            public void error(URL url) {
                try {
                    output.println("<span style=\"color: red\">Cannot index " + url + "</span>");
                    output.flush();
                } catch (Exception e) {
                }
            }
        });

        try {
            out.println("Start Indexing..." + "<br/>");
            out.flush();
            indexer.index(new URL(url));
            out.println("Done." + "<br/>");
        } catch (MalformedURLException ex) {
            out.println("<span style=\"color: red\">ERROR: invalid url</span>" + "<br/>");
        } finally {
            db.close();
        }
    } else {
%>
<form method="post" action="spider.jsp">
    <input id="query" type="hidden" name="run" value="true">
    <button id="submit" type="submit" value="Submit">Run Spider</button>
</form>
<% } %>
</body>
</html>
