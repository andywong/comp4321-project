<%@ page import="java.io.File" %>
<%@ page import="java.util.*" %>
<%@ page import="comp4321.utilities.StopStem" %>
<%@ page import="comp4321.utilities.PorterStopStem" %>
<%@ page import="comp4321.utilities.Pair" %>
<%@ page import="comp4321.PageMetaInfo" %>
<%@ page import="comp4321.SearchEngine" %>
<%@ page import="comp4321.Database" %>
<%@ page import="comp4321.Spider" %>

<!DOCTYPE html>
<html>
<head>
    <title>COMP4321 Project - Search Results</title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    
<div class="header-search">
    <h1>
        <a href="comp4321proj.html">
            <span style="color:blue">CO</span><span style="color:orange">MP</span><span style="color:red">43</span><span style="color:blue">21</span><span style="color:green"> Pro</span><span style="color:red">ject</span>
        </a>
    </h1>
    <div class="search">
        <form method="get" action="results.jsp">
            <input id="query" type="text" name="query" value="<%= request.getParameter("query").replaceAll("\"", "&quot;") %>">
            <button id="submitbutton" type="submit" value="Submit">Search</button>
        </form>
    </div>
</div>

<div class="results">
<%
    if(request.getParameter("query") != null) {
        long start = System.currentTimeMillis();
   
        String jspFilePath = getServletContext().getRealPath(request.getServletPath());
        Database db = new Database(new File(new File(jspFilePath).getParent(), "/database/db"));
        StopStem stemmer = new PorterStopStem(Spider.class.getResourceAsStream("stopwords.txt"));

        SearchEngine se = new SearchEngine(db, stemmer);
        List<Pair<Long, Double>> results = se.query(request.getParameter("query"));

        double searchTime = (System.currentTimeMillis() - start) / 1000.0;
        out.println("<span class=\"result-info\">Found " + results.size() + " result(s) for query: <span class=\"query-str\">" + request.getParameter("query") + "</span> (" + String.format("%.2f", searchTime) + " seconds)</span>");
    
        Map<Long, PageMetaInfo> pageInfoMap = db.getPageInfoMap();
        Map<Long, Set<Long>> childrenMap = db.getChildrenMap(), parentsMap = db.getParentsMap();
        Map<Long, List<Pair<Long, List<Integer>>>> forwardIndex = db.getForwardIndex();
    
        String url, top_results, keywords_html, word;
        PageMetaInfo info;
        List<Pair<Long,  List<Integer>>> wordsFreq;
        for (Pair<Long, Double> e : results) {
            url = db.getPageUrl(e.first);
            info = pageInfoMap.get(e.first);
            wordsFreq = forwardIndex.get(e.first);
            
            int i = 0;
            top_results = "";
            keywords_html = "";
            for (Pair<Long,  List<Integer>> pair : wordsFreq) {
                word = db.getWord(pair.first);
                top_results += word;
                keywords_html += String.format("<a href=\"results.jsp?query=%1$s\" class=\"keyword\">%1$s</a> %2$d", word, pair.second.size());
                if (++i == 5) break;
                top_results += " ";
                keywords_html += "; ";
            }
    
%>
            <div class="result clearfix">
                <span class="score"><%= String.format("%.4f", e.second) %></span>
                <div class="details">
                    <a class="title" href="<%= url %>"><%= info.getTitle() %></a>
                    <span class="url"><%= url %> <a class="similar" href="results.jsp?query=<%= top_results %>">Similar</a></span>
                    <span class="meta-info"><%= new Date(info.getLastModified()) %>, <%= String.format("%.1f", info.getSize() / 1024.0) %> KB</span>
                    <span class="keywords"><%= keywords_html %></span>
                    <div class="popup">
                        <span class="name">Parent links</span>
                        <div class="popup-details">
<%
            for (long parentId : parentsMap.get(e.first)) {
                out.println("<span class=\"url\">" + db.getPageUrl(parentId) + "</span>");
            }   
%>
                        </div>
                    </div>
                    <div class="popup">
                        <span class="name">Child links</span>
                        <div class="popup-details">
<% 
            for (long childId : childrenMap.get(e.first)) {
                out.println("<span class=\"url\">" + db.getPageUrl(childId) + "</span>");
            }   
%>
                        </div>
                    </div>
                </div>
            </div>
<%
        }
    } else {
        response.sendRedirect("comp4321proj.html");
    }

%>
</div>
        
<div class="footer">
    <span>COMP 4321 project by Ng Yee Ting (ytngaa@ust.hk), Wong Kwok Shiu (kswongab@ust.hk), Wong Shirley Mei Wah (smwwong@ust.hk)</span>
</div>
                        
</body>
</html>
