package comp4321;

import comp4321.utilities.PorterStopStem;
import comp4321.utilities.StopStem;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Entry class for the spider
 * @author Andy K.S. Wong
 */
public class Spider {
    private static String url = "http://www.cse.ust.hk/";
    private static int indexLimit = 0;
    private static String dbFile = "db";

    public static void main(String[] args) {
        if (args.length >= 2) {
            url = args[0];
            indexLimit = Integer.parseInt(args[1]);
        }

        if (args.length >= 3) {
            dbFile = args[2];
        }

        Crawler crawler = new BreadthFirstCrawler(indexLimit);
        final Database db = new Database(new File(dbFile));
        StopStem stemmer = new PorterStopStem(Spider.class.getResourceAsStream("stopwords.txt"));

        Indexer indexer = new Indexer(crawler, db, stemmer);
        indexer.setListener(new Indexer.IndexListener() {
            @Override
            public void added(long id) {
                System.out.println("Indexed " + db.getPageUrl(id));
            }

            @Override
            public void updated(long id) {
                System.out.println("Updated index for " + db.getPageUrl(id));
            }

            @Override
            public void error(URL url) {
                System.err.println("Cannot index " + url);
            }
        });

        try {
            System.out.println("Start Indexing...");
            indexer.index(new URL(url));
            System.out.println("Done.");
        } catch (MalformedURLException ex) {
            System.err.println("ERROR: invalid url");
        } finally {
            db.close();
        }
    }
}
