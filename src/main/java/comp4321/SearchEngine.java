package comp4321;

import comp4321.utilities.Pair;
import comp4321.utilities.StopStem;

import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * The search engine
 * @author Andy K.S. Wong
 */
public class SearchEngine {
    public static final double TITLE_WEIGHT = 1.0;

    protected int resultLimit = 50;
    protected Database db;
    protected StopStem stemmer;

    private ConcurrentNavigableMap<Long, List<Pair<Long, List<Integer>>>> forwardIndex, forwardTitleIndex;
    private ConcurrentNavigableMap<Long, List<Pair<Long, Integer>>> invertedIndex, invertedTitleIndex;

    public SearchEngine(Database db, StopStem stemmer) {
        this.db = db;
        this.stemmer = stemmer;

        forwardIndex = db.getForwardIndex();
        forwardTitleIndex = db.getForwardTitleIndex();
        invertedIndex = db.getInvertedIndex();
        invertedTitleIndex = db.getInvertedTitleIndex();
    }

    public void setResultLimit(int limit) {
        this.resultLimit = limit;
    }

    public int getResultLimit() {
        return resultLimit;
    }

    /**
     * Perform a search by input string
     */
    public List<Pair<Long, Double>> query(String input) {
        Query q = Query.parse(input, stemmer);
        return query(q);
    }

    /**
     * Perform a search
     */
    protected List<Pair<Long, Double>> query(Query q) {
        if (q.isEmpty()) {
            return new ArrayList<Pair<Long, Double>>();
        }

        Double score;
        Map<Long, Double> docCosSim = getCosSim(q, forwardIndex, invertedIndex),
                titleCosSim = getCosSim(q, forwardTitleIndex, invertedTitleIndex);

        // Adjust doc score for title match
        for (Map.Entry<Long, Double> e : titleCosSim.entrySet()) {
            if ((score = docCosSim.get(e.getKey())) == null) {
                score = e.getValue();
            } else {
                score = (e.getValue() * TITLE_WEIGHT + score) / (TITLE_WEIGHT + 1);
            }
            docCosSim.put(e.getKey(), score);
        }

        // Sort the documents by rank
        List<Map.Entry<Long, Double>> docSimList = new ArrayList<Map.Entry<Long, Double>>(docCosSim.entrySet());
        Collections.sort(docSimList, new Comparator<Map.Entry<Long, Double>>() {
            public int compare(Map.Entry<Long, Double> e1, Map.Entry<Long, Double> e2) {
                return e2.getValue().compareTo(e1.getValue());
            }
        });

        // Filter the result
        List<Pair<Long, Double>> result = new ArrayList<Pair<Long, Double>>(resultLimit);
        for (Map.Entry<Long, Double> e : docSimList) {
            if (e.getValue() > 0 && result.size() < resultLimit) {
                result.add(Pair.from(e.getKey(), e.getValue()));
            } else {
                break;
            }
        }

        return result;
    }

    protected Map<Long, Double> getCosSim(Query q, ConcurrentNavigableMap<Long, List<Pair<Long, List<Integer>>>> fIndex, ConcurrentNavigableMap<Long, List<Pair<Long, Integer>>> iIndex) {
        Map<Long, Double> cosSimMap = getCosSim(q.getTerms(), fIndex, iIndex);
        List<Map<Long, Double>> phraseCosSimMap = new ArrayList<Map<Long, Double>>(q.getPhrases().size());
        List<List<String>> phrases = new ArrayList<List<String>>(q.getPhrases().size());
        phrases.addAll(q.getPhrases());

        for (List<String> phrase : phrases) {
            phraseCosSimMap.add(getCosSim(phrase, fIndex, iIndex));
        }

        // Combine scores
        List<Pair<Long, List<Integer>>> forwardPosting;
        Map<Long, Double> simMap;
        Double weight;
        for (int i = 0; i < phrases.size(); ++i) {
            simMap = phraseCosSimMap.get(i);

            for (Map.Entry<Long, Double> e : simMap.entrySet()) {
                forwardPosting = fIndex.get(e.getKey());
                if (hasPhrase(forwardPosting, phrases.get(i))) {
                    if ((weight = cosSimMap.get(e.getKey())) != null) {
                        weight += e.getValue();
                    } else {
                        weight = e.getValue();
                    }
                    cosSimMap.put(e.getKey(), weight);
                }
            }
        }

        // Normalize scores
        for (Map.Entry<Long, Double> e : cosSimMap.entrySet()) {
            e.setValue(e.getValue() / (phrases.size() + 1));
        }

        return cosSimMap;
    }

    protected Map<Long, Double> getCosSim(Collection<String> terms, ConcurrentNavigableMap<Long, List<Pair<Long, List<Integer>>>> fIndex, ConcurrentNavigableMap<Long, List<Pair<Long, Integer>>> iIndex) {
        Map<Long, Double> cosSimMap = new HashMap<Long, Double>();
        List<Pair<Long, Integer>> posting;
        Long termId;
        double score, weight, q_len = Math.sqrt(terms.size());
        int N = fIndex.size();

        for (String term : terms) {
            if ((termId = db.getWordId(term)) == null || (posting = iIndex.get(termId)) == null) {
                continue;
            }
            for (Pair<Long, Integer> entry : posting) {
                score = (cosSimMap.containsKey(entry.first) ? cosSimMap.get(entry.first) : 0.0);
                weight = idf(posting.size(), N) * entry.second;
                cosSimMap.put(entry.first, score + weight);
            }
        }

        // divide all weights by max tf and normalize by |Q| and |D|
        List<Pair<Long, List<Integer>>> forwardPosting;
        for (Map.Entry<Long, Double> e : cosSimMap.entrySet()) {
            forwardPosting = fIndex.get(e.getKey());
            score = e.getValue() / maxTf(forwardPosting) / q_len / docLen(forwardPosting, iIndex, N);
            e.setValue(score);
        }

        return cosSimMap;
    }

    protected boolean hasPhrase(List<Pair<Long, List<Integer>>> forwardPosting, List<String> phrase) {
        Long termId;
        int pos = -1;
        List<Long> termIds = new ArrayList<Long>(phrase.size());
        for (String term : phrase) {
            if ((termId = db.getWordId(term)) == null) {
                return false;
            } else {
                termIds.add(termId);
            }
        }

        return hasPhrase(forwardPosting, termIds, 0, -1);
    }

    private boolean hasPhrase(List<Pair<Long, List<Integer>>> forwardPosting, List<Long> termIds, int i, int pos) {
        if (i == termIds.size()) {
            return true;
        }

        long id = termIds.get(i);
        List<Integer> plist = null;
        for (Pair<Long, List<Integer>> p : forwardPosting) {
            if (p.first.equals(id)) {
                plist = p.second;
                break;
            }
        }

        if (plist != null) {
            for (int p : plist) {
                if (pos < 0) {
                    if (hasPhrase(forwardPosting, termIds, i + 1, p + 1)) {
                        return true;
                    }
                } else if (p == pos) {
                    return hasPhrase(forwardPosting, termIds, i + 1, p + 1);
                }
            }
        }

        return false;
    }

    protected double idf(int df, int N) {
        return Math.log(N * 1.0 / df) / Math.log(2);
    }

    protected double docLen(List<Pair<Long, List<Integer>>> forwardPosting, ConcurrentNavigableMap<Long, List<Pair<Long, Integer>>> iIndex, int N) {
        int tfMax = maxTf(forwardPosting);
        double len = 0, weight = 0;

        for (Pair<Long, List<Integer>> p : forwardPosting) {
            weight = p.second.size() * idf(iIndex.get(p.first).size(), N);
            len += weight * weight;
        }

        return Math.sqrt(len) / tfMax;
    }

    protected int maxTf(List<Pair<Long, List<Integer>>> forwardPosting) {
        int tfMax = 0;
        for (Pair<Long, List<Integer>> p : forwardPosting) {
            if (p.second.size() > tfMax) {
                tfMax = p.second.size();
            }
        }

        return tfMax;
    }
}
