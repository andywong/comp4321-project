package comp4321;

import org.mapdb.BTreeMap;
import comp4321.utilities.Pair;
import comp4321.utilities.PorterStopStem;
import comp4321.utilities.StopStem;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * Test program for search engine
 * @author Andy K.S. Wong
 */
public class TestSearch {
    public static void main(String[] args) throws IOException {
        try {
            Database db = new Database(new File("db"));
            BTreeMap<Long, PageMetaInfo> pageInfoMap = db.getPageInfoMap();
            StopStem stemmer = new PorterStopStem(Spider.class.getResourceAsStream("stopwords.txt"));

            SearchEngine se = new SearchEngine(db, stemmer);
            List<Pair<Long, Double>> results;

            Scanner in = new Scanner(System.in);

            while (true) {
                System.out.print("Search: ");
                results = se.query(in.nextLine());

                System.out.println("Found " + results.size() + " results.");
                for (Pair<Long, Double> e : results) {
                    System.out.println(e.second + " " + pageInfoMap.get(e.first).getTitle());
                    System.out.println(db.getPageUrl(e.first));
                    System.out.println();
                }
            }
        } finally {
            System.out.println("Done.");
        }
    }
}
