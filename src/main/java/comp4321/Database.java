package comp4321;

import org.mapdb.*;
import comp4321.utilities.Pair;

import java.io.Closeable;
import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Andy K.S. Wong
 */
public class Database implements Closeable {
    protected DB db;

    private Atomic.Long pageCounter, wordCounter;

    private HTreeMap<String, Long> pageIdLookup, wordIdLookup;
    private BTreeMap<Long, String> pageUrlLookup, wordLookup;

    private BTreeMap<Long, PageMetaInfo> pageInfoMap;

    private BTreeMap<Long, Set<Long>> childrenMap, parentsMap;

    private BTreeMap<Long, List<Pair<Long, List<Integer>>>> forwardIndex;
    private BTreeMap<Long, List<Pair<Long, Integer>>> invertedIndex;

    private BTreeMap<Long, List<Pair<Long, List<Integer>>>> forwardTitleIndex;
    private BTreeMap<Long, List<Pair<Long, Integer>>> invertedTitleIndex;

    public Database(File dbFile) {
        db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .mmapFileEnable()
                .make();

        init();
    }

    protected void init() {
        pageIdLookup = db.getHashMap("pageIdLookup");
        pageUrlLookup = db.getTreeMap("pageUrlLookup");
        Bind.mapInverse(pageIdLookup, pageUrlLookup);

        wordIdLookup = db.getHashMap("wordIdLookup");
        wordLookup = db.getTreeMap("wordLookup");
        Bind.mapInverse(wordIdLookup, wordLookup);

        pageInfoMap = db.getTreeMap("pageInfoMap");

        childrenMap = db.getTreeMap("childrenMap");
        parentsMap = db.getTreeMap("parentsMap");

        forwardIndex = db.getTreeMap("forwardIndex");
        invertedIndex = db.getTreeMap("invertedIndex");

        forwardTitleIndex = db.getTreeMap("forwardTitleIndex");
        invertedTitleIndex = db.getTreeMap("invertedTitleIndex");

        pageCounter = db.getAtomicLong("pageCounter");
        wordCounter = db.getAtomicLong("wordCounter");
    }

    public void commit() {
        db.commit();
    }

    @Override
    public void close() {
        db.close();
    }

    public long getPageId(String url) {
        Long id;
        if ((id = pageIdLookup.get(url)) == null) {
            id = nextPageId();
            pageIdLookup.put(url, id);
        }

        return id;
    }

    public long getPageId(URL url) {
        return getPageId(url.toString());
    }

    public Collection<Long> getPageIds() {
        return pageInfoMap.keySet();
    }

    public String getPageUrl(long id) {
        return pageUrlLookup.get(id);
    }

    public boolean isPageIndexed(String url) {
        Long id;
        return ((id = pageIdLookup.get(url)) != null) && pageInfoMap.containsKey(id);
    }

    public long getWordId(String word) {
        Long id;
        if ((id = wordIdLookup.get(word)) == null) {
            id = nextWordId();
            wordIdLookup.put(word, id);
        }

        return id;
    }

    public String getWord(long id) {
        return wordLookup.get(id);
    }

    public BTreeMap<Long, PageMetaInfo> getPageInfoMap() {
        return pageInfoMap;
    }

    public BTreeMap<Long, Set<Long>> getChildrenMap() {
        return childrenMap;
    }

    public BTreeMap<Long, Set<Long>> getParentsMap() {
        return parentsMap;
    }

    public BTreeMap<Long, List<Pair<Long, List<Integer>>>> getForwardIndex() {
        return forwardIndex;
    }

    public BTreeMap<Long, List<Pair<Long, Integer>>> getInvertedIndex() {
        return invertedIndex;
    }

    public BTreeMap<Long, List<Pair<Long, List<Integer>>>> getForwardTitleIndex() {
        return forwardTitleIndex;
    }

    public BTreeMap<Long, List<Pair<Long, Integer>>> getInvertedTitleIndex() {
        return invertedTitleIndex;
    }

    protected long nextPageId() {
        return pageCounter.incrementAndGet();
    }

    protected long nextWordId() {
        return wordCounter.incrementAndGet();
    }
}
