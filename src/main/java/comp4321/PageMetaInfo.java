package comp4321;

import java.io.Serializable;

/**
 * Contains the meta info of a page
 * @author Andy K.S. Wong
 */
public class PageMetaInfo implements Serializable {
    protected String title;
    protected long lastModified = 0, size = 0;
    protected boolean visited = true;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    /**
     * For internal use only
     */
    public boolean isVisited() {
        return visited;
    }

    /**
     * For internal use only
     */
    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}