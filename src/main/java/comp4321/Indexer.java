package comp4321;

import comp4321.utilities.Pair;
import comp4321.utilities.StopStem;

import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * The main class for creating indexes
 * @author Andy K.S. Wong
 */
public class Indexer {
    protected Crawler crawler;
    protected Database db;
    protected StopStem stemmer;

    protected IndexListener listener = new IndexListener() {
        @Override
        public void added(long id) {
        }

        @Override
        public void updated(long id) {
        }

        @Override
        public void error(URL url) {

        }
    };

    public Indexer(Crawler crawler, Database db, StopStem stemmer) {
        this.crawler = crawler;
        this.db = db;
        this.stemmer = stemmer;
    }

    public void setListener(IndexListener l) {
        if (l != null) {
            listener = l;
        }
    }

    public void index(URL url) {
        ConcurrentNavigableMap<Long, PageMetaInfo> pageInfoMap = db.getPageInfoMap();
        ConcurrentNavigableMap<Long, Set<Long>> childrenMap = db.getChildrenMap();
        ConcurrentNavigableMap<Long, List<Pair<Long, List<Integer>>>> forwardIndex = db.getForwardIndex(), forwardTitleIndex = db.getForwardTitleIndex();

        for (PageMetaInfo info : pageInfoMap.values()) {
            info.setVisited(false);
        }

        Page page = new Page(url);
        crawler.add(page);
        while (crawler.hasNext()) {
            page = crawler.next();

            try {
                page.loadMetaInfo();
            } catch (Exception ex) {
                listener.error(page.getUrl());
                continue;
            }

            String pageUrl = page.getUrl().toString();
            boolean indexed = db.isPageIndexed(pageUrl);
            long id = db.getPageId(pageUrl);
            PageMetaInfo info = pageInfoMap.get(id);

            if (!indexed || (!info.isVisited() && page.getLastModified() > info.getLastModified())) {
                page.parse();

                pageInfoMap.put(id, page.getMetaInfo());

                List<Pair<Long, List<Integer>>> keyWordList = buildWordList(page.getWords());
                Collections.sort(keyWordList, new Comparator<Pair<Long, List<Integer>>>() {
                    public int compare(Pair<Long, List<Integer>> o1, Pair<Long, List<Integer>> o2) {
                        return (o2.second.size() - o1.second.size());
                    }
                });
                forwardIndex.put(id, keyWordList);

                keyWordList = buildWordList(page.getTitleWords());
                forwardTitleIndex.put(id, keyWordList);

                Set<Long> children = new TreeSet<Long>();
                for (URL child : page.getLinks()) {
                    children.add(db.getPageId(child));
                }
                childrenMap.put(id, children);

                if (!indexed) {
                    listener.added(id);
                } else {
                    listener.updated(id);
                }

                crawler.accept();
            }
        }

        buildInvertedIndex();
        buildParentsMap();

        db.commit();
    }

    protected void buildInvertedIndex() {
        ConcurrentNavigableMap<Long, List<Pair<Long, List<Integer>>>> forwardIndex = db.getForwardIndex(), forwardTitleIndex = db.getForwardTitleIndex();
        ConcurrentNavigableMap<Long, List<Pair<Long, Integer>>> invertedIndex = db.getInvertedIndex(), invertedTitleIndex = db.getInvertedTitleIndex();
        List<Pair<Long, List<Integer>>> forwardList;
        List<Pair<Long, Integer>> invertedList;

        invertedIndex.clear();
        invertedTitleIndex.clear();

        for (long id : forwardIndex.navigableKeySet()) {
            forwardList = forwardIndex.get(id);
            for (Pair<Long, List<Integer>> post : forwardList) {
                if ((invertedList = invertedIndex.get(post.first)) == null) {
                    invertedList = new LinkedList<Pair<Long, Integer>>();
                }
                invertedList.add(Pair.from(id, post.second.size()));
                invertedIndex.put(post.first, invertedList);
            }
        }

        for (long id : forwardTitleIndex.navigableKeySet()) {
            forwardList = forwardTitleIndex.get(id);
            for (Pair<Long, List<Integer>> post : forwardList) {
                if ((invertedList = invertedTitleIndex.get(post.first)) == null) {
                    invertedList = new LinkedList<Pair<Long, Integer>>();
                }
                invertedList.add(Pair.from(id, post.second.size()));
                invertedTitleIndex.put(post.first, invertedList);
            }
        }
    }

    protected void buildParentsMap() {
        ConcurrentNavigableMap<Long, Set<Long>> parentsMap = db.getParentsMap(), childrenMap = db.getChildrenMap();
        Set<Long> parents;

        parentsMap.clear();

        for (long parentId : db.getPageIds()) {
            for (long childId : childrenMap.get(parentId)) {
                if ((parents = parentsMap.get(childId)) == null) {
                    parents = new TreeSet<Long>();
                }
                parents.add(parentId);
                parentsMap.put(childId, parents);
            }
        }
    }

    protected List<Pair<Long, List<Integer>>> buildWordList(List<String> tokens) {
        Map<Long, List<Integer>> tf = new ConcurrentSkipListMap<Long, List<Integer>>();
        long id;
        List<Integer> occurList;
        String word;

        int i = 0;
        for (String token : tokens) {
            word = stemmer.stopStem(token);
            if (word != null) {
                id = db.getWordId(word);
                if ((occurList = tf.get(id)) == null) {
                    occurList = new ArrayList<Integer>();
                }
                occurList.add(i);
                tf.put(id, occurList);
            }
            ++i;
        }

        List<Pair<Long, List<Integer>>> tfList = new LinkedList<Pair<Long, List<Integer>>>();
        for (Map.Entry<Long, List<Integer>> e : tf.entrySet()) {
            tfList.add(Pair.from(e.getKey(), e.getValue()));
        }

        return tfList;
    }

    /**
     * A listener for index update events
     */
    public static interface IndexListener {
        /**
         * Called when new page added to index
         * @param id id of the new page
         */
        void added(long id);

        /**
         * Called when an indexed page is updated
         * @param id id of the updated page
         */
        void updated(long id);

        /**
         * Called when a page cannot be indexed
         * @param url URL of the page
         */
        void error(URL url);
    }
}
