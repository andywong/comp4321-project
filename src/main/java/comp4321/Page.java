package comp4321;

import org.htmlparser.Node;
import org.htmlparser.Parser;
import org.htmlparser.beans.LinkBean;
import org.htmlparser.beans.StringBean;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.util.ParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A crawled page
 * @author Andy K.S. Wong
 */
public class Page {
    private URL url;
    private String title = null;

    private List<String> words = null, titleWords = null;
    private List<URL> links = null;

    private long lastModified = 0, contentLength = -1;

    public Page(URL url) {
        this.url = url;
    }

    public URL getUrl() {
        return url;
    }

    public void parse() {
        if (words == null || links == null) {
            StringBean sb = new StringBean();
            sb.setLinks(false);
            sb.setURL(url.toString());

            // remove non-word and split
            String[] tokens = sb.getStrings().replaceAll("&.*?;", "").split("\\W+");

            words = new ArrayList<String>(tokens.length);
            for (String token : tokens) {
                if (!token.isEmpty()) {
                    words.add(token.toLowerCase());
                }
            }

            LinkBean lb = new LinkBean();
            lb.setURL(url.toString());
            URL[] ls = lb.getLinks();

            links = new ArrayList<URL>(ls.length);
            Collections.addAll(links, ls);
        }

        if (title != null && titleWords == null) {
            String[] tokens = title.replaceAll("&.*?;", "").split("\\W+");

            titleWords = new ArrayList<String>(tokens.length);
            for (String token : tokens) {
                if (!token.isEmpty()) {
                    titleWords.add(token.toLowerCase());
                }
            }
        }
    }

    public void loadMetaInfo() throws ParserException, IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        lastModified = conn.getLastModified();
        if (lastModified == 0) {
            lastModified = conn.getDate();
        }

        contentLength = conn.getContentLength();
        if (contentLength < 0) {
            String inputLine;
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            try {
                while ((inputLine = in.readLine()) != null) {
                    contentLength += inputLine.length();
                }
            } finally {
                in.close();
            }
        }

        if (title == null) {
            Parser parser = new Parser();
            parser.setURL(url.toString());

            Node node = parser.extractAllNodesThatMatch(new TagNameFilter("title")).elementAt(0);
            if(node != null) {
                title = node.toPlainTextString();
            } else {
                title = "";
            }
        }
    }

    public PageMetaInfo getMetaInfo() {
        PageMetaInfo info = new PageMetaInfo();
        info.setTitle(title);
        info.setLastModified(lastModified);
        info.setSize(contentLength);

        return info;
    }

    public long getLastModified() {
        return lastModified;
    }

    public List<String> getWords() {
        return words;
    }

    public List<String> getTitleWords() {
        return titleWords;
    }

    public List<URL> getLinks() {
        return links;
    }
}
