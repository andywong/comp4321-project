package comp4321;

import comp4321.utilities.Pair;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Test program to dump the pages information in database
 * @author Andy K.S. Wong
 */
public class Dump {
    private static String fileName = "spider_result.txt";

    public static void main(String[] args) throws IOException {
        if (args.length > 0) {
            fileName = args[0];
        }

        PrintWriter writer = new PrintWriter(new File(fileName));
        System.out.println("Dumping indexed pages details to " + fileName + "...");

        try {
            Database db = new Database(new File("db"));
            ConcurrentMap<Long, PageMetaInfo> pageInfoMap = db.getPageInfoMap();
            ConcurrentMap<Long, Set<Long>> childrenMap = db.getChildrenMap();
            ConcurrentNavigableMap<Long, List<Pair<Long, List<Integer>>>> forwardIndex = db.getForwardIndex();
            //ConcurrentNavigableMap<Long, List<Pair<Long, List<Integer>>>> forwardTitleIndex = db.getForwardTitleIndex();

            List<Pair<Long,  List<Integer>>> wordsFreq;

            PageMetaInfo curr;
            for (long pageId : db.getPageIds()) {
                curr = pageInfoMap.get(pageId);

                writer.println(curr.getTitle());
                writer.println(db.getPageUrl(pageId));
                writer.print(new Date(curr.getLastModified()));
                writer.print(", ");
                writer.print(curr.getSize());
                writer.println(" Bytes");

                /*wordsFreq = forwardTitleIndex.get(pageId);
                for (Pair<Long,  List<Integer>> pair : wordsFreq) {
                    writer.print(db.getWord(pair.first) + " " + pair.second.size());
                    writer.print("; ");
                }
                writer.println();*/

                wordsFreq = forwardIndex.get(pageId);
                int i = 0;
                for (Pair<Long,  List<Integer>> pair : wordsFreq) {
                    writer.print(db.getWord(pair.first) + " " + pair.second.size());
                    if (++i == 5) break;
                    writer.print("; ");
                }
                writer.println();

                for (long childId : childrenMap.get(pageId)) {
                    writer.println(db.getPageUrl(childId));
                }
                writer.println("-------------------------------------------------------------------------------------------");
            }
        } finally {
            System.out.println("Done.");
            writer.close();
        }
    }
}
