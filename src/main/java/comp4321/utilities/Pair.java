package comp4321.utilities;

import java.io.Serializable;

/**
 * A pair of objects
 * @author Andy K.S. Wong
 */
public class Pair<A, B> implements Serializable {
    public A first;
    public B second;

    public Pair(A first, B second) {
        this.first = first;
        this.second = second;
    }

    public static <A, B> Pair<A, B> from(A first, B second) {
        return new Pair<A, B>(first, second);
    }
}

