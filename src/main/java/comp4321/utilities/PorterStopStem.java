package comp4321.utilities;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Porter stemming with stopword list
 * @author Andy K.S. Wong
 */
public class PorterStopStem implements StopStem {
    private Porter porter;
    private Set<String> stopWords;

    public PorterStopStem() {
        porter = new Porter();
        stopWords = new HashSet<String>();
    }

    public PorterStopStem(InputStream input) {
        this();
        loadStopWords(input);
    }

    /**
     * Load stopwords from input stream
     * @param input
     */
    public void loadStopWords(InputStream input) {
        Scanner stopWordsFile = new Scanner(input);

        while (stopWordsFile.hasNext()) {
            stopWords.add(stopWordsFile.nextLine());
        }
    }

    @Override
    public boolean isStopWord(String str) {
        return stopWords.contains(str);
    }

    @Override
    public String stem(String str) {
        if (str != null && !str.isEmpty()) {
            return porter.stripAffixes(str);
        }

        return null;
    }

    @Override
    public String stopStem(String str) {
        return (isStopWord(str) ? null : stem(str));
    }
}

