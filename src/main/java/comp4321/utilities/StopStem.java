package comp4321.utilities;

/**
 * A utility interface for handling stopwords and stemming
 * @author Andy K.S. Wong
 */
public interface StopStem {
    /**
     * Check if str is a stop word
     * @param str the word
     */
    boolean isStopWord(String str);

    /**
     * Stem the word str
     * @param str
     */
    String stem(String str);

    /**
     * Perform both stemming and cutting stopword
     * @param str
     * @return null if str is a stopword; the stemmed string otherwise
     */
    String stopStem(String str);
}
