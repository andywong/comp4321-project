package comp4321;

import java.net.URL;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Breadth-first strategy for traversing the web
 * @author Andy K.S. Wong
 */
public class BreadthFirstCrawler implements Crawler {
    protected Queue<Page> pages = new LinkedList<Page>();
    protected Page current = null;

    protected int processed = 0;
    protected int limit;

    public BreadthFirstCrawler() {
        this(0);
    }

    /**
     * @param limit maximum number of pages to be cached; non-positive value means unrestricted
     */
    public BreadthFirstCrawler(int limit) {
        this.limit = limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public void add(Page page) {
        pages.add(page);
    }

    @Override
    public void accept() {
        if (current != null) {
            ++processed;
            current.parse();
            for (URL url : current.getLinks()) {
                pages.add(new Page(url));
            }
        }
    }

    @Override
    public void clear() {
        pages.clear();
    }

    @Override
    public boolean hasNext() {
        return (pages.size() > 0 && (limit <= 0 || processed < limit));
    }

    @Override
    public Page next() {
        if (pages.size() > 0) {
            current = pages.remove();
            return current;
        }
        return null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove");
    }
}
