package comp4321;

import comp4321.utilities.StopStem;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Represent a search query
 * @author Andy K.S. Wong
 */
public class Query {
    private Set<String> terms;
    private Set<List<String>> phrases;

    protected Query() {
        terms = new HashSet<String>();
        phrases = new HashSet<List<String>>();
    }

    /**
     * Parse the query string into unique terms and list of phrases
     * @param queryStr
     * @return
     */
    public static Query parse(String queryStr, StopStem stemmer) {
        Query query = new Query();
        String term, word;
        Pattern token = Pattern.compile("\"\\w*\"?|\\w+");
        Pattern phrase_token = Pattern.compile("\\w*\"|\\w+");

        Scanner s = new Scanner(queryStr.replaceAll("&.*?;", ""));

        while (s.hasNext(token)) {
            term = s.next(token);
            if (term.startsWith("\"") && !(term.length() > 1 && term.endsWith("\""))) {
                List<String> phrase = new ArrayList<String>();

                if (term.length() > 1) {
                    word = stemmer.stopStem(term.substring(1));
                    if (word != null) {
                        phrase.add(word);
                    }
                }

                while (s.hasNext(phrase_token)) {
                    term = s.next(phrase_token);
                    if (term.endsWith("\"")) {
                        if (term.length() > 1) {
                            word = stemmer.stopStem(term.substring(0, term.length() - 1));
                            if (word != null) {
                                phrase.add(word);
                            }
                        }
                        break;
                    } else {
                        word = stemmer.stopStem(term);
                        if (word != null) {
                            phrase.add(word);
                        }
                    }
                }

                query.phrases.add(phrase);
                //query.terms.addAll(phrase);
            } else {
                word = stemmer.stopStem(term.replace("\"", ""));
                if (word != null) {
                    query.terms.add(word);
                }
            }
        }

        return query;
    }

    public Set<String> getTerms() {
        return terms;
    }

    public Set<List<String>> getPhrases() {
        return phrases;
    }

    public boolean isEmpty() {
        return terms.size() + phrases.size() == 0;
    }
}
