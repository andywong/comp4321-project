package comp4321;

import java.util.Iterator;

/**
 * The crawler interface which represents a strategy to traverse the web
 * @author Andy K.S. Wong
 */
public interface Crawler extends Iterator<Page> {
    void add(Page page);
    void clear();
    void accept();
}
