COMP 4321 Project
Group 16
Ng Yee Ting <ytngaa@ust.hk>, Wong Kwok Shiu <kswongab@ust.hk>, Wong Shirley Mei Wah <smwwong@ust.hk>

This project is managed by maven.

To build the project, use:
mvn compile

To run the spider, use:
mvn exec:java -Dexec.mainClass=comp4321.Spider -Dexec.args="http://www.cse.ust.hk/ 30"

The above command will run the spider that begins at http://www.cse.ust.hk/ and indexes 30 pages.

To run the test program, use:
mvn exec:java -Dexec.mainClass=comp4321.Dump

The data from database will be outputted to spider_result.txt by default.

